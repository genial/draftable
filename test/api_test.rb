# frozen_string_literal: true
#
require 'time'
require 'byebug'
require 'minitest/autorun'
require 'minitest/assertions'
require 'draftable'
require 'faraday'
require 'webmock/minitest'
require 'uri'


class ApiTest < Minitest::Test

  def setup
    Draftable.reset
    Draftable.configure do |config|
      config.api_key = 'xxx'
      config.account = 'zzz'
    end unless Draftable.configured?
    @api = Draftable::Api.new
  end

  def teardown
    File.unlink('old_file.pdf') if File.exist?('old_file.pdf')
    File.unlink('new_file.pdf') if File.exist?('new_file.pdf')
  end

  # webmock can't match multipart upload forms so I can't add this to my stub
  def test_should_build_request_with_two_files
    old_file = File.open('old_file.pdf', 'wb+')
    new_file = File.open('new_file.pdf', 'wb+')
    assert Draftable.configuration.valid?
    stub_request(:post, 'https://api.draftable.com/comparisons')
      .with(headers: {'Authorization' => "Token xxx",
                      "Content-Type" => "multipart/form-data"})
      .to_return(status: 201,
                 body: {
                   count: 1,
                   limit: 0,
                   offset: 0,
                   results: [
                     {
                       identifier: "token-xx",
                       creation_time: Time.now.utc.iso8601(3),
                       expiry_time: ' ',
                       public: false,
                       ready: true
                     }
                   ]}.to_json)
    api_response = @api.create(old_file: old_file, new_file: new_file, token: 'token-xx')
    assert_equal 'token-xx', api_response
  end

  def test_should_create_comparison_with_2_urls
    old_file = URI.parse('https://s3.probably.amazon.com/bucket/old_file.pdf')
    new_file = URI.parse('https://s3.probably.amazon.com/bucket/new_file.pdf')
    assert Draftable.configuration.valid?
    stub_request(:post, 'https://api.draftable.com/comparisons')
      .with(headers: {'Authorization' => "Token xxx",
                      "Content-Type" => "application/json"})
      .to_return(status: 201,
                 body: {
                   count: 1,
                   limit: 0,
                   offset: 0,
                   results: [
                     {
                       identifier: "token-xx",
                       creation_time: Time.now.utc.iso8601(3),
                       expiry_time: ' ',
                       public: false,
                       ready: true
                     }
                   ]}.to_json)
    api_response = @api.create(old_file: old_file, new_file: new_file, token: 'token-xx')
    assert_equal 'token-xx', api_response
  end

  def test_should_check_status_of_a_comparison
    # pending
    stub_request(:get, 'https://api.draftable.com/comparisons/token-xx')
      .with(headers: {'Authorization' => "Token xxx",
                      "Content-Type" => "application/json"})
      .to_return(status: 200,
                 body: {identifier: "token-xx",
                        left: {
                          source_url: "https://www.testurl.com/old-example.pdf",
                          display_name: "old-example.pdf",
                          file_type: "pdf"
                        },
                        right: {
                          source_url: "https://www.testurl.com/newer-example.pdf",
                          display_name: "newer-example.pdf",
                          file_type: "pdf"
                        },
                        creation_time: Time.now.utc.iso8601(3),
                        expiry_time: '',
                        public: false,
                        ready: false
                 }.to_json)
    # ready
    stub_request(:get, 'https://api.draftable.com/comparisons/token-yy')
      .with(headers: {'Authorization' => "Token xxx",
                      "Content-Type" => "application/json"})
      .to_return(status: 200,
                 body: {identifier: "token-yy",
                        left: {
                          source_url: "https://www.testurl.com/old-example.pdf",
                          display_name: "old-example.pdf",
                          file_type: "pdf"
                        },
                        right: {
                          source_url: "https://www.testurl.com/newer-example.pdf",
                          display_name: "newer-example.pdf",
                          file_type: "pdf"
                        },
                        creation_time: Time.now.utc.iso8601(3),
                        expiry_time: '',
                        public: false,
                        ready: true,
                        failed: false
                 }.to_json)

    # failed
    stub_request(:get, 'https://api.draftable.com/comparisons/token-zz')
      .with(headers: {'Authorization' => "Token xxx",
                      "Content-Type" => "application/json"})
      .to_return(status: 200,
                 body: {identifier: "token-zz",
                        left: {
                          source_url: "https://www.testurl.com/old-example.pdf",
                          display_name: "old-example.pdf",
                          file_type: "pdf"
                        },
                        right: {
                          source_url: "https://www.testurl.com/newer-example.pdf",
                          display_name: "newer-example.pdf",
                          file_type: "pdf"
                        },
                        creation_time: Time.now.utc.iso8601(3),
                        expiry_time: '',
                        public: false,
                        ready: true,
                        failed: true
                 }.to_json)

    assert_equal :pending, @api.status('token-xx')
    assert_equal :ready, @api.status('token-yy')
    assert_equal :failed, @api.status('token-zz')
  end

  def test_should_display_public_comparison
    assert_equal 'https://api.draftable.com/comparisons/viewer/zzz/token-yy', @api.comparison('token-yy')
  end

  def test_should_display_private_comparison
    valid_until = (Time.now + 60).to_i
    payload = "zzzxxxtoken-yy#{valid_until.to_s}"
    signature = OpenSSL::HMAC.hexdigest('SHA256', 'xxx', payload)
    uri = URI.parse('https://api.draftable.com/comparisons/viewer/zzz/token-yy')
    uri.query = "valid_until=#{valid_until}&signature=#{signature}"

    assert_equal uri.to_s, @api.private_comparison('token-yy', valid_until)
  end

  def test_should_build_post_body_for_url_source
    old_file = 'https://very.secure.file_hosting.com/old_file.pdf'
    new_file = 'https://very.secure.file_hosting.com/new_file.pdf'
    expected_output = {
      left: {file_type: 'pdf',
             source_url: old_file,
             display_name: 'Old Version'},
      right: {file_type: 'pdf',
              source_url: new_file,
              display_name: 'New Version'},
      public: false
    }
    assert_equal expected_output, @api.send(:build_post_request_body, URI.parse(old_file), URI.parse(new_file), nil)
  end

  def test_should_build_post_body_for_upload_source
    old_file = File.open('old_file.pdf', 'wb+')
    new_file = File.open('new_file.pdf', 'wb+')
    expected_output = {
      left: {file_type: 'pdf',
             display_name: 'old_file.pdf'},
      right: {file_type: 'pdf',
              display_name: 'new_file.pdf'},
      public: false
    }
    output = @api.send(:build_post_request_body, old_file, new_file, nil)
    assert !output[:public]
    assert_equal 'pdf', output[:left][:file_type]
    assert_equal 'old_file.pdf', output[:left][:display_name]
    assert_equal 'pdf', output[:right][:file_type]
    assert_equal 'new_file.pdf', output[:right][:display_name]
  end
end
