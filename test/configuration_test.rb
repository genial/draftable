# frozen_string_literal: true
require 'minitest/autorun'
require 'minitest/assertions'
require 'draftable'

class DraftableTest < Minitest::Test

  def test_should_ensure_the_configuration_is_correct
    Draftable.reset
    config = Draftable.configuration
    original_uri = config.api_url
    assert !config.valid?, "The errors were #{config.errors}"
    # the default URI will be used so it's not listed as an error
    assert_equal 2, config.errors.size
    assert_includes "API Key is missing", config.errors[:api_key]
    assert_includes "API Account is missing", config.errors[:account]

    config.api_key = 'xxx'
    config.account = 'zzz'
    config.api_url = nil
    assert !config.valid?
    assert_equal 1, config.errors.size
    assert_equal 'API URL is missing', config.errors[:api_url]

    config.api_url = original_uri
    assert config.valid?
  end
end
