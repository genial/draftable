# Draftable
This gem provides an interface to the Draftable document comparison API.

The API documentation can be found here: [Draftable API](https://api.draftable.com/)

## Installation
For Rails apps just add it to your `Gemfile`
```ruby
gem 'draftable'
```
and run `bundle install`

For any other ruby application first you have to install the gem
```bash
gem install draftable
```
and then require it in your app
```ruby
require 'draftable'
```

## Usage
To use this gem you have to first configure it so that the API knows where to connect to and what credentials to use
```ruby
Draftable.configure do |config|
  config.api_key = 'xxx'
  config.account = 'your draftable account id'
  config.api_url = 'your.instance.address'
end
```

The `api_url` is optional and only required when you run a self-hosted instance of Draftable. It defaults to `https://api.draftable.com`.

### Create comparison
To compare 2 files you can pass either an instance of `URI` or an instance of `IO` like the `File` class. 

```ruby
old_file = File.open('old_file.pdf')
new_file = File.open('new_file.pdf')
token = Draftable.compare(old_doc: old_file, new_doc: new_file)
```
The above will create a comparison and will return the token used to fetch the comparison. 

The comparison can take a few seconds so it's better to generate a unique token and send it with your request. That way Draftable will not block the connection and you can redirect your user to the viewer and show them the loading screen.

```ruby
old_file = URI.parse('https://s3.amazon.com/bucket-name/old_file.pdf')
new_file = URI.parse('https://s3.amazon.com/bucket-name/new_file.pdf')
unique_token = SecureRandom.uuid
token = Draftable.compare(old_doc: old_file, new_doc: new_file, token: unique_token)
```
w
All comparisons are created as `private` by default. To view the private comparison 
```ruby
token = 'token-id'
url = Draftable.private_comparison(token, (Time.now + 60).to_i)
```

and to view the public comparison
```ruby
token = 'token-id'
url = Draftable.comparison(token)
```

This will return a link that you can embed in your `iframe` or open in a new window/tab.

## Limitations
  * At the moment the gem only supports comparison of PDF files.
  * The gem only supports comparison via URL. Local files can't be uploaded for comparison with this gem at the moment. 
  * The comparison deletion is not implemented yet.
  * Also, the public comparisons are not supported just yet.

## License
MIT
