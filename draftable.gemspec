Gem::Specification.new do |spec|
  spec.name        = 'draftable'
  spec.version     = '0.1.1'
  spec.date        = '2021-04-19'
  spec.summary     = "API interface for Draftable document comparison"
  spec.description = "This gem provides easy to use integration with Draftable document comparison API"
  spec.authors     = ["Mirek Sawicz"]
  spec.email       = 'mirek.sawicz@gmail.com'
  spec.files       = ["lib/draftable.rb", "lib/draftable/api.rb", 'lib/draftable/configuration.rb']
  spec.license       = 'MIT'
  spec.add_development_dependency 'minitest', '>= 5.8.4'
  spec.add_development_dependency 'bundler', '>= 2.1.4'
  spec.add_development_dependency 'webmock', '>= 3.8.3'
  spec.add_runtime_dependency 'faraday', '>= 1.0'
  spec.required_ruby_version = '>=2.6'
end
