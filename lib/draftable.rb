# frozen_string_literal: true
require 'faraday'
require 'draftable/api'
require 'draftable/configuration'

module Draftable

  class << self
    attr_accessor :configuration
  end

  def self.compare(old_file:, new_file:, token: nil, left_title: nil, right_title: nil)
    api.create(old_file: old_file, new_file: new_file, token: token, left_title: left_title, right_title: right_title)
  end

  def self.status(token)
    api.status(token)
  end

  def self.comparison(token)
    api.comparison(token)
  end

  def self.private_comparison(token, valid_until)
    api.private_comparison(token, valid_until)
  end

  protected

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.reset
    @configuration = nil
  end

  def self.configure
    yield(configuration)
  end

  def self.configured?
    self.configuration.valid?
  end

  private

  def self.api
    @api ||= Api.new
  end
end
