# frozen_string_literal: true
require 'openssl'
require 'json'
require 'base64'

module Draftable
  class Api
    attr_reader :api_key, :api_url, :account

    def initialize
      @api_key = Draftable.configuration.api_key
      @api_url = Draftable.configuration.api_url
      @account = Draftable.configuration.account
    end

    def create(old_file:, new_file:, token: nil, left_title: nil, right_title: nil)
      post('/comparisons', build_post_request_body(old_file, new_file, token, left_title, right_title))['identifier']
    end

    # returns a URL to a public comparison as a string
    def comparison(token)
      "#{@api_url}/comparisons/viewer/#{account}/#{token}"
    end

    # returns a URL to a private comparison as a string
    def private_comparison(token, valid_until)
      payload = [account, token, valid_until].to_json
      signature = OpenSSL::HMAC.hexdigest('SHA256', api_key, payload)
      uri = URI.parse("#{@api_url}/comparisons/viewer/#{account}/#{token}")
      uri.query = "valid_until=#{valid_until}&signature=#{signature}"
      uri.to_s
    end

    def status(token)
      response = get('/comparisons/' + token)
      is_ready = response['ready']
      has_failed = response['failed']
      if is_ready and has_failed
        :failed
      elsif is_ready and !has_failed
        :ready
      else
        :pending
      end
    end

    private

    def get(path, params = {})
      url = "#{@api_url}#{path}"
      response = Faraday.new(url, ssl: {verify: false}).get do |req|
        req.params = params
        req.headers = set_headers({})
      end

      if response.status == 200
        JSON.parse(response.body)
      else
      raise StandardError, "GET request to #{url} failed with status #{response.status}. Response body: #{response.body}"
      end
    end

    def post(path, body)
      url = @api_url + path
      request = Faraday.new(url, ssl: {verify: false}) do |f|
        f.request :multipart
        f.request :url_encoded
        f.adapter :net_http
      end

      response = request.post(url, body.to_json, set_headers(body))

      if response.status == 201
        JSON.parse(response.body)
      else
      raise StandardError, "POST request to #{url} failed with status #{response.status}. Response body: #{response.body}"
      end
    end

    def build_post_request_body(old_file, new_file, token, left_title, right_title)
      post_body = default_post_body
      post_body[:left] = add_side_file(old_file)
      post_body[:right] = add_side_file(new_file)
      post_body[:left][:display_name] = left_title.blank? ? '' : left_title
      post_body[:right][:display_name] = right_title.blank? ? '' : right_title
      post_body[:token] = token if token
      post_body
    end

    # currently we only support PDF documents
    def default_post_body
      {
        left: {},
        right: {},
        public: false
      }
    end

    def add_side_file(file)
      side = {}
      side[:file_type] = 'pdf'
      if file.is_a?(IO)
        side[:file] = Faraday::UploadIO.new(file, 'application/pdf', File.basename(file.path))
        side[:display_name] = File.basename(file.path)
      elsif file.is_a? URI
        side[:source_url] = file.to_s
      else
        raise ArgumentError.new("The comparison file has to be an instance of IO or URI")
      end
      side
    end

    def set_headers(body)
      headers = {:Authorization => "Token #{api_key}"}
      if body.dig(:right, :file) or body.dig(:left, :file)
        headers["Content-Type"] = "multipart/form-data"
      else
        headers["Content-Type"] = "application/json"
      end
      headers
    end
  end
end
