# frozen_string_literal: true
require 'uri'
# The configuration should contain the following options
#   @api_key: an api key for your account
#   @api_url: this is a url to the API that you're connecting to. This should be passed as a URI instance
#   @account: is your Draftable account number
#
module Draftable
  class Configuration

    attr_accessor :errors

    HOSTED_API_URL = 'https://api.draftable.com'

    attr_accessor :api_key,
                  :api_url,
                  :account

    def initialize
      @account = nil
      @api_key = nil
      @api_url = HOSTED_API_URL
    end

    def valid?
      @errors = {}
      if @api_key.nil? || @api_key.empty?
        @errors[:api_key] = "API Key is missing"
      end

      if @account.nil? || @account.empty?
        @errors[:account] = 'API Account is missing'
      end

      if @api_url.nil?
        @errors[:api_url] = 'API URL is missing'
      end
      @errors.none?
    end

  end
end
